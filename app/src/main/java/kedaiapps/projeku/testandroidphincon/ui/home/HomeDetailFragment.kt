package kedaiapps.projeku.testandroidphincon.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import kedaiapps.projeku.testandroidphincon.R
import kedaiapps.projeku.testandroidphincon.common.UiState
import kedaiapps.projeku.testandroidphincon.databinding.FragmentHomeDetailBinding
import kedaiapps.projeku.testandroidphincon.ext.observe
import kedaiapps.projeku.testandroidphincon.ext.toast
import kedaiapps.projeku.testandroidphincon.modules.base.BaseFragment
import kedaiapps.projeku.testandroidphincon.services.entity.ResponseHomeDetail
import kedaiapps.projeku.testandroidphincon.ui.home.adapter.AdapterHomeDetail
import kedaiapps.projeku.testandroidphincon.ui.home.adapter.AdapterHomeDetailMoves
import kedaiapps.projeku.testandroidphincon.ui.home.adapter.AdapterHomeDetailTypes
import kedaiapps.projeku.testandroidphincon.viewmodel.MainViewModel
import kedaiapps.projeku.testandroidphincon.viewmodel.RepoViewModel

class HomeDetailFragment : BaseFragment() {
    lateinit var mBinding: FragmentHomeDetailBinding
    private val viewModel by viewModels<MainViewModel>()
    private val viewModelRepo by viewModels<RepoViewModel>()
    private val args by navArgs<HomeDetailFragmentArgs>()

    private val adapter by lazy(LazyThreadSafetyMode.NONE) {
        AdapterHomeDetail()
    }

    private val adapterMoves by lazy(LazyThreadSafetyMode.NONE) {
        AdapterHomeDetailMoves()
    }

    private val adapterTypes by lazy(LazyThreadSafetyMode.NONE) {
        AdapterHomeDetailTypes()
    }

    private var id = ""
    private var background_image = ""
    private var name = ""
    private var weight = ""
    private var status = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        mBinding = FragmentHomeDetailBinding.inflate(inflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbar()
        initView()
        handleState()
    }

    private fun initToolbar() {
        mBinding.tlbr.apply {
            ivBack.setOnClickListener {
                findNavController().popBackStack()
            }
            tvTitle.setOnClickListener {
                findNavController().popBackStack()
            }
        }
    }

    private fun initView() {
        mBinding.progress.progressBar.setAnimation(R.raw.loading)

        viewModel.homeDetail(args.name)

        mBinding.rv.adapter = adapter
        mBinding.rvMoves.adapter = adapterMoves
        mBinding.rvTypes.adapter = adapterTypes

        id = args.id
        getFavorite(id)

        mBinding.btnSubmit.setOnClickListener {
            val dialog = DialogPickme(requireActivity())
            dialog.listener = object : DialogPickme.Listener{
                override fun onYes(data: Int) {
                    if (status != "") {
                        status = (status.toInt() + 1).toString()
                        name += "-$status"

                        viewModelRepo.updateFavorite(id, name, status)
                        requireContext().toast("$name berhasil ditambahkan")
                    }else{
                        name += "-0"
                        viewModelRepo.setFavorite(id, background_image, name, weight, "0")
                        requireContext().toast("$name berhasil ditambahkan")
                    }

                    findNavController().popBackStack()
                }
            }
            dialog.show()
        }
    }

    private fun handleState() {
        observe(viewModel.responseHomeDetail) {
            if (it != null) {
                setData(it)
            }
        }

        // loading
        observe(viewModel.loadState) {
            when (it) {
                UiState.Loading -> mBinding.progress.progressBar.isVisible = true
                UiState.Success -> {
                    mBinding.progress.progressBar.isVisible = false
                }
                is UiState.Error -> {
                    mBinding.progress.progressBar.isVisible = false
                    requireActivity().toast(it.message)
                }
            }
        }
    }

    private fun getFavorite(id: String){
        observe(viewModelRepo.getFavoriteId(id)) {
            if (it != null) {
                status = it.status
            }
        }
    }

    private fun setData(data: ResponseHomeDetail) {
        background_image = data.sprites.other.home.front_default
        name = data.name
        weight = data.weight.toString()

        mBinding.tlbr.tvTitle.text = data.name

        Glide.with(this).load(data.sprites.other.home.front_default)
            .apply(
                RequestOptions()
                    .transform(RoundedCorners(16))
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .dontAnimate()
            ).into(mBinding.image)

        mBinding.judul.text = data.name
        mBinding.weight.text = "Berat : "+data.weight

        adapter.clearData()
        adapter.insertData(data.abilities)

        adapterMoves.clearData()
        adapterMoves.insertData(data.moves)

        adapterTypes.clearData()
        adapterTypes.insertData(data.types)
    }
}