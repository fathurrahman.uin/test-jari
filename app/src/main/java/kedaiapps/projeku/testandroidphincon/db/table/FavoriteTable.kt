package kedaiapps.projeku.testandroidphincon.db.table

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class FavoriteTable(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val fav_id: String,
    val background_image: String,
    val name: String,
    val weight: String,
    val status: String,
)